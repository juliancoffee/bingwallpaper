#!/usr/bin/python

"""
Python script to set bing image of the day as desktop wallpaper
Based on https://github.com/anuragrana/Python-Scripts

LICENCE
Original Code Copyright 2020 Anurag Rana
Modified Work Copyright 2020 Illia Denysenko

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import json
import argparse
import re
import os
import sys
import random
from pathlib import Path
from typing import TypedDict, Optional

import requests


LANGUAGES = [
    "auto",
    "en-US",
    "zh-CN",
    "ja-JP",
    "en-AU",
    "en-UK",
    "de-DE",
    "en-NZ",
    "en-CA",
]


class Config(TypedDict):
    lang: str
    store: str
    cmd: str
    verbosity: int
    need_all: bool


def cli_args() -> Config:
    exe = Path(sys.argv[0]).name
    examples = f"""
examples:
    # download picture of the day to current directory with messages what's
    # going on
        $ {exe} -v
    # download pictures for all languages to current directory
        $ {exe} -va
    # download picture of the day to given directory
        $ {exe} -s $HOME/Images/Wallpapers
    # download picture of the day to given directory
        $ {exe} -s $HOME/Images/Wallpapers
    # download picture of the day to current directory and set it as wallpaper
        $ {exe} -c 'nitrogen --save --set-centered {{filepath}}'
    # open new files in imv
        $ {exe} -a -c 'imv -f {{filepaths}}'
    # russian roulette (with -a flag, {exe} is using random file for
      {{filepath}})
        $ {exe} -a -c 'nitrogen --save --set-centered {{filepath}}'
    """

    parser = argparse.ArgumentParser(
        epilog=examples,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("--lang", type=str, choices=LANGUAGES,
                        default="auto",
                        help="specify language (by default auto)")
    parser.add_argument("-s", "--store", type=str,
                        default=".",
                        help="specify path to store image "
                             "(default: $PWD)")
    parser.add_argument("-c", "--command", metavar="CMD", type=str,
                        default="",
                        help="specify command to call as callback"
                             "- e.g, set wallpaper")
    parser.add_argument("-a", "--all", action="store_true",
                        help="dowload all possible images")
    parser.add_argument("-v", action="store_true",
                        help="be verbose")
    parser.add_argument("--verbosity", type=int, choices=[0, 1],
                        default=0,
                        help="set verbosity level")

    args = parser.parse_args()
    verbosity = 1 if args.v else args.verbosity
    return {
        "lang": args.lang,
        "store": args.store,
        "cmd": args.command,
        "verbosity": verbosity,
        "need_all": args.all,
    }


def configuration() -> Config:
    # From command-line args
    res = cli_args()
    return res


def debug(config: Config, message: str):
    verbosity = config["verbosity"]
    if verbosity >= 1:
        print(message)


def error(message: str):
    print(message)


def get_image_url(lang: str) -> str:
    """ Get full url to download image """

    response = requests.get(
        "https://www.bing.com/HPImageArchive.aspx?"
        f"format=js&idx=0&n=1&mkt={lang}"
    )
    image_data = json.loads(response.text)

    image_url: str = image_data["images"][0]["url"]
    full_image_url = "https://www.bing.com" + image_url
    return full_image_url


# Get image name
def get_image_name(url: str, conf: Config) -> str:
    image_url = url.split("&")[0]
    # first pattern for matching beginning of the url
    # second pattern to match image name
    # third for the rest
    image_name_pattern = \
        r"th\?id=OHR\." + \
        "([^_]*)" + \
        ".*"
    image_name = re.findall(image_name_pattern, image_url)[0]
    image_extension = image_url.split(".")[-1]
    image_file = f"{image_name}.{image_extension}"

    debug(conf, message=f"Image name {image_name}")
    return image_file


def download_url(url: str, conf: Config) -> bytes:
    debug(conf, message=f"Getting: {url}")
    return requests.get(url).content


def save_img(img_data, path_to_store, conf):
    debug(conf, message=f"Saving to {path_to_store}")
    with open(path_to_store, 'wb') as dest:
        dest.write(img_data)


def process(lang: str, conf: Config) -> Optional[str]:
    """ Download and save image """
    debug(conf, f"\t{lang}")
    full_image_url = get_image_url(lang)
    image_name = get_image_name(full_image_url, conf)
    store_place = conf["store"]
    path_to_store = Path(store_place, image_name)

    # Check if file don't exists already
    if not path_to_store.is_file():
        img_data = download_url(full_image_url, conf)
        save_img(img_data, path_to_store, conf)
        debug(conf, f"Saved\n")
        return str(path_to_store)
    else:
        debug(conf, f"File exists skip\n")
        return None


def main():
    config = configuration()
    chosen_language = config["lang"]
    cmd = config["cmd"]
    need_all = config["need_all"]
    paths = []
    if need_all:
        # Download wallpaper for all languages
        for language in LANGUAGES:
            path = process(language, config)
            paths.append(path)

    else:
        # Download wallpaper for specific language
        path = process(chosen_language, config)
        paths.append(path)

    # Call CMD if provided with needed arguments
    ready = [path for path in paths if path is not None]
    path_to_store = "" if ready == [] else random.choice(ready)
    files = " ".join(ready)
    if "{filepaths}" in cmd and files == "":
        error(f"Trying to call cmd with filepaths, but no new files are ready")
        sys.exit()
    if "{filepath}" in cmd and path_to_store == "":
        error(f"Trying to call cmd with filepath, but no new files are ready")
        sys.exit()
    command_string = cmd.format(filepath=path_to_store, filepaths=files)
    print(command_string)
    os.system(command_string)


if __name__ == "__main__":
    main()
