### Usage:
main.py [-h] [--lang {auto,en-US,zh-CN,ja-JP,en-AU,en-UK,de-DE,en-NZ,en-CA}] [-s STORE] [-c CMD] [-a] [-v] [--verbosity {0,1}]

### Options:
| Argument | |
-----------|----
|`-h, --help` | show this help message and exit |
|`--lang {auto,en-US,zh-CN,ja-JP,en-AU,en-UK,de-DE,en-NZ,en-CA}`| specify language (by default auto)|
|`-s STORE, --store STORE` |specify path to store image (default: $PWD)|
|`-c CMD, --command CMD` |specify command to call with resulted file - e.g, set wallpaper|
|`-a, --all` | dowload all possible images|
|`-v` | be verbose|
|`--verbosity {0,1}` | set verbosity level|

### Examples:
- download picture of the day to current directory with messages what's going on <br>
`$ ./main.py -v`
- download pictures for all languages to current directory <br>
`$ ./main.py -va`
- download picture of the day to given directory<br>
`$ ./main.py -s $HOME/Images/Wallpapers`
- download picture of the day to given directory<br>
`$ ./main.py -s $HOME/Images/Wallpapers `
- download picture of the day to given directory and set it as wallpaper (nitrogen must be installed)<br>
`$ ./main.py -s $HOME/Wallpapers -c 'nitrogen --save --set-auto {file}' `
